import 'react-native-gesture-handler';
import React, {useContext, useEffect} from 'react';
import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import MainBottomNavigators from './src/navigators/MainBottomNavigators';
import {LoginContext} from './src/context/loginContext';
import {API} from './src/config/api';
import AsyncStorage from '@react-native-community/async-storage';
import AuthNavigators from './src/navigators/AuthNavigators';
import SplashScreen from './src/screens/SplashScreen';

const _getToken = async () => {
  try {
    const token = await AsyncStorage.getItem('token');
    if (token !== null) {
      setAuthToken(token);
    }
  } catch (error) {}
};

const Stack = createStackNavigator();
const App = () => {
  const [state, dispatch] = useContext(LoginContext);
  _getToken();
  useEffect(() => {
    const loadUser = async () => {
      try {
        const res = await API.get('/auth');
        dispatch({
          type: 'USER_LOADED',
          payload: res.data.data.user,
        });
      } catch (err) {
        dispatch({
          type: 'AUTH_ERROR',
        });
      }
    };
    loadUser();
  }, [dispatch]);

  const MyTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: 'rgb(255, 45, 85)',
      background: 'black',
    },
  };
  return (
    <NavigationContainer theme={MyTheme}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          animationEnabled: false,
        }}>
        {state.loading ? (
          <Stack.Screen name="Splash" component={SplashScreen} />
        ) : state.isLogin ? (
          <Stack.Screen name="Main" component={MainBottomNavigators} />
        ) : (
          <Stack.Screen name="Login" component={AuthNavigators} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
