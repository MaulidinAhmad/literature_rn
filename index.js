/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {LoginContextProvider} from './src/context/loginContext';
import React from 'react';

const Root = () => (
  <LoginContextProvider>
    <App />
  </LoginContextProvider>
);

AppRegistry.registerComponent(appName, () => Root);
