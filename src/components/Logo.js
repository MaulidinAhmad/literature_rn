import React from 'react';
import {Image} from 'react-native';
import LogoImage from '../assets/image/Icon.png';
function Logo(props) {
  return <Image {...props} source={LogoImage} />;
}

export default Logo;
