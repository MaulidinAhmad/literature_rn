import React from 'react';
import {Button} from 'react-native-elements';

function ButtonCustom(props) {
  return (
    <Button
      {...props}
      title="Submit"
      buttonStyle={{padding: 15, backgroundColor: '#AF2E1C'}}
    />
  );
}

export default ButtonCustom;
