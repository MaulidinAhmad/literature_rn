import React from 'react';
import {StyleSheet} from 'react-native';
import {Header, Icon} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
function CustomHeader(props) {
  const navigation = useNavigation();
  return (
    <Header
      containerStyle={styles.header}
      leftComponent={{
        icon: 'arrow-left',
        type: 'font-awesome',
        color: 'white',
        size: 18,
        onPress: () => navigation.goBack(),
      }}
      centerComponent={{
        text: props.title,
        style: {color: 'white', fontSize: 20},
      }}
    />
  );
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#252525',
    borderBottomWidth: 0.5,
    borderBottomColor: '#454545',
  },
});
export default CustomHeader;
