import React from 'react';
import {Text} from 'react-native-elements';

function TitleText(props) {
  return (
    <Text
      style={{
        fontSize: 28,
        color: 'white',
        margin: 10,
        textAlign: 'center',
        fontWeight: 'bold',
      }}>
      {props.title}
    </Text>
  );
}

export default TitleText;
