import React, {useContext} from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, Image, Text, Icon, Divider} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import CustomHeader from '../components/CustomHeader';
import TitleText from '../components/TitleText';
import {imageUrl} from '../config/api';
import {LoginContext} from '../context/loginContext';

function ProfileScreen(props) {
  const [state, dispatch] = useContext(LoginContext);
  const handlePress = () => {
    dispatch({
      type: 'LOGOUT',
    });
  };
  return (
    <ScrollView>
      <View style={{flex: 1}}>
        <CustomHeader title="Profile" />
        <View style={styles.container}>
          <View style={styles.headContainer}>
            <View style={{justifyContent: 'center', marginRight: 20}}>
              <Image
                source={{uri: `${imageUrl}/${state.user.img}`}}
                style={{width: 100, height: 100, borderRadius: 100 / 2}}
              />
            </View>
            <View style={{justifyContent: 'center'}}>
              <Text style={styles.profileTextStyle}>{state.user.fullName}</Text>
              <Text style={{color: 'gray', fontSize: 16}}>
                {state.user.gender}
              </Text>
            </View>
          </View>
          <View style={styles.contactConatiner}>
            <View style={styles.contactListStyle}>
              <View style={{marginRight: 20}}>
                <Icon
                  name="envelope"
                  color="white"
                  iconStyle={styles.iconStyle}
                  type="font-awesome"
                />
              </View>
              <View>
                <Text style={styles.contactTextStyle}>{state.user.email}</Text>
              </View>
            </View>
            <View style={styles.contactListStyle}>
              <View style={{marginRight: 25}}>
                <Icon
                  name="phone"
                  color="white"
                  iconStyle={styles.iconStyle}
                  type="font-awesome"
                />
              </View>
              <View>
                <Text style={styles.contactTextStyle}>{state.user.phone}</Text>
              </View>
            </View>

            <View style={styles.contactListStyle}>
              <View style={{marginRight: 20}}>
                <Icon
                  name="map"
                  color="white"
                  iconStyle={styles.iconStyle}
                  type="font-awesome"
                />
              </View>
              <View>
                <Text style={styles.contactTextStyle}>
                  {state.user.address}
                </Text>
              </View>
            </View>
          </View>
          <Divider
            style={{backgroundColor: '#252525', width: '100%', height: 2}}
          />
          <View style={styles.profileTextStyle}>
            <Button
              containerStyle={styles.buttonStyle}
              titleStyle={{color: '#AF2E1C'}}
              icon={{
                name: 'power-off',
                color: '#AF2E1C',
                type: 'font-awesome',
              }}
              title="Log Out"
              type="clear"
              onPress={handlePress}
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headContainer: {
    width: '100%',
    flex: 1 / 2,
    paddingHorizontal: 20,
    marginTop: 30,
    flexDirection: 'row',
    alignContent: 'center',
  },
  profileTextStyle: {
    width: '100%',
    marginTop: 4,
    textAlign: 'left',
    color: 'white',
    fontSize: 19,
  },
  contactTextStyle: {
    marginTop: 4,
    textAlign: 'left',
    color: 'white',
    fontSize: 14,
  },
  contactListStyle: {
    flexDirection: 'row',
    marginBottom: 14,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  contactConatiner: {
    width: '100%',
    flex: 1 / 2,
    marginTop: 20,
    paddingLeft: 20,
    paddingVertical: 4,
    width: '100%',
    alignItems: 'flex-start',
  },
  iconStyle: {
    textAlign: 'left',
  },
  buttonStyle: {
    marginTop: 10,
    paddingLeft: 10,
    flexDirection: 'row',
    width: '100%',
    alignItems: 'flex-start',
  },
});

export default ProfileScreen;
