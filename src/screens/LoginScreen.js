import React, {useContext, useState} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input, Button} from 'react-native-elements';
import ButtonCustom from '../components/ButtonCustom';
import Logo from '../components/Logo';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {LoginContext} from '../context/loginContext';
import {API, headerConfig, setAuthToken} from '../config/api';
import {ScrollView} from 'react-native-gesture-handler';

function LoginScreen(props) {
  const [state, dispatch] = useContext(LoginContext);
  const [loading, setloading] = useState(false);
  const [trueLogin, settrueLogin] = useState(false);

  const initialValues = {
    email: '',
    password: '',
  };
  const Schema = Yup.object().shape({
    email: Yup.string().required('Email Not Allow To Empty').email(),
    password: Yup.string().required('Password Not Allow To Empty').min(8),
  });

  const handleSubmit = async (e, action) => {
    setloading(true);
    const body = JSON.stringify(e);
    // console.log(body);
    try {
      const res = await API.post('/login', body, headerConfig);
      dispatch({
        type: 'LOGIN_SUCCESS',
        payload: res.data.data,
      });
      setAuthToken(res.data.data.token);
      // settrueLogin(false);
      // setloading(false);
      try {
        const res = await API.get('/auth');
        dispatch({type: 'USER_LOADED', payload: res.data.data.user});
      } catch (error) {
        dispatch({type: 'AUTH_ERROR'});
      }
    } catch (error) {
      dispatch({
        type: 'LOGIN_FAIL',
      });
      // settrueLogin(true);
      setloading(false);
    }
  };
  return (
    <ScrollView>
      <View style={styles.container}>
        {/* <View style={styles.logo}>
          <Logo />
        </View> */}
        <View style={styles.formContainer}>
          <Formik
            initialValues={initialValues}
            validationSchema={Schema}
            onSubmit={(e, action) => {
              handleSubmit(e, action);
            }}>
            {(formik) => {
              const {
                errors,
                touched,
                handleBlur,
                handleSubmit,
                handleChange,
                values,
              } = formik;
              return (
                <View>
                  <Text style={styles.textLogin}>LOGIN</Text>
                  <Input
                    errorMessage={errors.email}
                    label="EMAIL"
                    placeholder="Input Email"
                    onChangeText={handleChange('email')}
                    value={values.email}
                    onBlur={handleBlur('email')}
                    leftIcon={{type: 'font-awesome', name: 'user'}}
                  />
                  <Input
                    errorMessage={errors.password}
                    label="PASSWORD"
                    placeholder="Input Password"
                    secureTextEntry={true}
                    value={values.password}
                    onBlur={handleBlur('password')}
                    onChangeText={handleChange('password')}
                    leftIcon={{type: 'font-awesome', name: 'key'}}
                  />
                  <ButtonCustom loading={loading} onPress={handleSubmit} />
                </View>
              );
            }}
          </Formik>
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>
              Have no account ? Click{' '}
              <Text
                style={{color: 'red'}}
                onPress={() => props.navigation.navigate('Signup')}>
                Here
              </Text>
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  textStyle: {
    color: 'white',
  },
  formContainer: {
    width: '95%',
    marginTop: '30%',
    // borderRadius: 40,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    backgroundColor: 'white',
    opacity: 0.9,
    paddingVertical: 30,
    paddingHorizontal: 15,
  },
  logo: {
    position: 'relative',
    margin: 0,
    marginBottom: 0,
  },
  textLogin: {
    fontSize: 30,
    textAlign: 'center',
    marginBottom: 25,
  },
});

export default LoginScreen;
