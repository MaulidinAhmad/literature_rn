import React from 'react';
import {Image, View} from 'react-native';
import Logo from '../assets/image/search_logo.png';
function SplashScreen(props) {
  return (
    <View
      style={{
        opacity: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
      }}>
      <Image
        resizeMode="contain"
        style={{flex: 1, aspectRatio: 0.5, resizeMode: 'contain'}}
        source={Logo}
      />
    </View>
  );
}

export default SplashScreen;
