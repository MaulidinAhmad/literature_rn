import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import {Button, SearchBar} from 'react-native-elements';
import {useQuery} from 'react-query';
import LiteraturesList from '../components/LiteratureList';
import Logo from '../components/Logo';
import {API} from '../config/api';
function HomeScreen(props) {
  const [keyword, setkeyword] = useState('');
  const [searched, setsearched] = useState(false);

  const {data: literatureData, refetch, isLoading, error} = useQuery(
    'getAllLiteratures',
    async () =>
      await API.get(
        `/literatures?status=Approved&name=${keyword}&page=1&limit=20`,
      ),
  );

  useEffect(() => {
    setkeyword(keyword);
    refetch();
    // console.log(literatureData.data.data);
  }, [keyword]);

  // console.log(literatureData.data.data);

  const handleSearchChange = (text) => {
    setkeyword(text);
    refetch();
  };

  const handleSearchSubmit = () => {
    setsearched(true);
  };

  return (
    <View style={{flex: 1}}>
      <View style={{marginTop: 20}}>
        <SearchBar
          containerStyle={searched ? {opacity: 1} : {opacity: 0}}
          placeholder="Type Here..."
          onChangeText={(text) => handleSearchChange(text)}
          value={keyword}
        />
      </View>
      <View style={searched ? {display: 'none'} : styles.mainSearch}>
        <Logo style={{aspectRatio: 4}} />
        <TextInput
          placeholder="Cari Literature Anda"
          placeholderTextColor="white"
          onChangeText={(text) => handleSearchChange(text)}
          value={keyword}
          style={{
            marginTop: 20,
            height: 40,
            width: '95%',
            borderColor: 'gray',
            borderWidth: 1,
            borderRadius: 4,
            borderColor: 'white',
            color: 'white',
          }}
          // onChangeText={(text) => onChangeText(text)}
          // value={value}
        />
        <Button
          color="#AF2E1C"
          title="Cari"
          onPress={handleSearchSubmit}
          buttonStyle={{paddingHorizontal: 20, marginTop: 20}}
        />
      </View>
      {isLoading ? (
        <Text>Loading...</Text>
      ) : error ? (
        <Text style={{color: 'white'}}>{error.message}</Text>
      ) : keyword === '' ? (
        <View></View>
      ) : (
        <View style={styles.container}>
          <Text
            style={{
              color: 'white',
              marginVertical: 20,
              marginLeft: 10,
            }}>
            Search Keyword "{keyword}"
          </Text>
          <LiteraturesList
            literatures={literatureData.data.data}
            isLoading={isLoading}
            refetch={refetch}
          />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

    // justifyContent: 'center',
    // alignItems: 'center',
  },
  mainSearch: {
    flex: 1,

    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default HomeScreen;
