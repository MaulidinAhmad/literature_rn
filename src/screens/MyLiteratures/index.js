import React from 'react';
import MyLiteratureScreen from './MyLiteratureScreen';
import {createStackNavigator} from '@react-navigation/stack';
import CollectionDetailScreen from '../Collections/CollectionDetailScreen';
function MyLiteratures(props) {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator
      initialRouteName="literatureList"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="literatureList" component={MyLiteratureScreen} />
      <Stack.Screen
        name="collectionDetail"
        component={CollectionDetailScreen}
      />
    </Stack.Navigator>
  );
}

export default MyLiteratures;
