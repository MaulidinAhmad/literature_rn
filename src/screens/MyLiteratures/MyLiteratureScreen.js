import React, {useEffect, useState} from 'react';
import {View, StyleSheet, SafeAreaView} from 'react-native';
import {API} from '../../config/api';
import {useQuery} from 'react-query';

import LiteraturesList from '../../components/LiteratureList';
import SplashScreen from '../SplashScreen';
import CustomHeader from '../../components/CustomHeader';
function MyLiteratureScreen(props) {
  const {data: literatureData, refetch, isLoading} = useQuery(
    'getLiteratures',
    async () => await API.get('/userliteratures'),
  );

  useEffect(() => {
    refetch();
  }, []);

  return isLoading ? (
    <SplashScreen />
  ) : (
    <View style={{flex: 1}}>
      <CustomHeader title="My Literature" />
      <View style={styles.container}>
        <LiteraturesList
          literatures={literatureData.data.data}
          isLoading={isLoading}
          refetch={refetch}
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#161616',
    color: 'white',
    padding: 15,
  },
});

export default MyLiteratureScreen;
