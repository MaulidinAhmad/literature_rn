import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import CustomHeader from '../../components/CustomHeader';
import {imageUrl} from '../../config/api';

function CollectionDetailScreen(props) {
  const {data} = props.route.params;
  return (
    <View style={{flex: 1}}>
      <CustomHeader title={data.title} />
      <ScrollView>
        <View style={Styles.container}>
          <Image
            source={{uri: `${imageUrl}/${data.file}.png`}}
            style={Styles.cover}
            resizeMode="contain"
          />
          <View style={Styles.detailContainer}>
            <Text style={Styles.titleStyle}>TITLE</Text>
            <Text style={Styles.textDataStyle}>{data.title}</Text>
            <Text style={Styles.titleStyle}>AUTHOR</Text>
            <Text style={Styles.textDataStyle}>{data.author}</Text>
            <Text style={Styles.titleStyle}>PAGES</Text>
            <Text style={Styles.textDataStyle}>{data.pages}</Text>
            <Text style={Styles.titleStyle}>ISBN</Text>
            <Text style={Styles.textDataStyle}>{data.ISBN}</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const Styles = StyleSheet.create({
  container: {
    marginVertical: 20,
    // width: '100%',
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  detailContainer: {
    marginTop: 20,
    paddingHorizontal: 10,
  },
  textStyle: {
    color: 'white',
  },
  titleStyle: {
    fontSize: 14,
    color: 'white',
  },
  textDataStyle: {
    fontSize: 24,
    color: 'white',
    marginBottom: 10,
  },
  cover: {
    width: '100%',
    height: 400,
  },
});

export default CollectionDetailScreen;
