import React from 'react';
import CollectionDetailScreen from './CollectionDetailScreen';
import MyCollectionScreen from './MyCollectionScreen';
import {createStackNavigator} from '@react-navigation/stack';
function Collections(props) {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator
      initialRouteName="collectionList"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="collectionList" component={MyCollectionScreen} />
      <Stack.Screen
        name="collectionDetail"
        component={CollectionDetailScreen}
      />
    </Stack.Navigator>
  );
}

export default Collections;
