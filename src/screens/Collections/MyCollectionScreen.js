import React, {useEffect, useState} from 'react';
import {View, StyleSheet, SafeAreaView} from 'react-native';
import {API} from '../../config/api';
import {useQuery} from 'react-query';

import LiteraturesList from '../../components/LiteratureList';
import SplashScreen from '../SplashScreen';
import CustomHeader from '../../components/CustomHeader';
function MyCollectionScreen(props) {
  const {data: collectionData, refetch, isLoading} = useQuery(
    'getCollections',
    async () => await API.get('/collections'),
  );

  let literatures;
  if (!isLoading) {
    literatures = collectionData.data.data.map((data) => data.literature);
  }

  // console.log(literatures);

  useEffect(() => {
    refetch();
  }, []);

  return isLoading ? (
    <SplashScreen />
  ) : (
    <View style={{flex: 1}}>
      <CustomHeader title="My Collection" />
      <View style={styles.container}>
        <LiteraturesList
          literatures={literatures}
          isLoading={isLoading}
          refetch={refetch}
          detailScreen="CollectionDetailScreen"
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#161616',
    color: 'white',
    padding: 15,
  },
});

export default MyCollectionScreen;
