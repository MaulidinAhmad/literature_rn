import {Formik} from 'formik';
import React, {useContext, useState} from 'react';
import {Text, View, StyleSheet, ToastAndroid} from 'react-native';
import {Button} from 'react-native-elements';
import CustomHeader from '../components/CustomHeader';
import * as Yup from 'yup';
import ButtonCustom from '../components/ButtonCustom';
import {Input} from 'react-native-elements';
import {Picker} from '@react-native-community/picker';
import {ScrollView} from 'react-native-gesture-handler';
import {LoginContext} from '../context/loginContext';
import {API} from '../config/api';
import DocumentPicker from 'react-native-document-picker';

function AddScreen(props) {
  const [loginState, dispatch] = useContext(LoginContext);
  const [loading, setloading] = useState(false);
  const [file, setfile] = useState(null);

  const initialValues = {
    title: '',
    publication: '',
    pages: '',
    ISBN: '',
    author: '',
    // file: null,
  };

  const Schema = Yup.object().shape({
    title: Yup.string().required('Title Not Allow To be Empty'),
    publication: Yup.string().required('Publication Not Allow To be Empty'),
    pages: Yup.number('Only Number Allowed').required(
      'Pages Not Allow To be Empty',
    ),
    ISBN: Yup.number('Only Number Allowed').required(
      'ISBN Not Allow To be Empty',
    ),
    author: Yup.string().required('Author Not Allow To be Empty'),
  });

  const chooseFile = async (action) => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
        //There can me more options as well
      });
      //Setting the state to show single file attributes
      setfile(res);
    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        alert('Canceled from single doc picker');
      } else {
        //For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  const handleSubmitForm = async (e, action) => {
    setloading(true);
    // const body = JSON.stringify(e);
    try {
      let body = new FormData();
      if (!loginState.loading) {
        if (loginState.user.isAdmin === '1') {
          body.append('status', 'Approved');
        }
      }
      body.append('title', e.title);
      body.append('publication', e.publication);
      body.append('pages', e.pages);
      body.append('ISBN', e.ISBN);
      body.append('author', e.author);
      body.append('file', file);

      const config = {
        headers: {
          'Content-type': 'multipart/form-data',
        },
      };
      // console.log(body);
      await API.post('/literature', body, config);
      // action.resetForm(initialValues);
      setloading(false);
      alert('ok');
      // ToastAndroid.show('Successfully Add Literature');
    } catch (error) {
      // settrueLogin(true);
      setloading(false);
    }
  };
  return (
    <View style={{flex: 1}}>
      <CustomHeader title="Add Literature" />
      <ScrollView>
        <View style={Styles.container}>
          <Formik
            initialValues={initialValues}
            validationSchema={Schema}
            onSubmit={(e, action) => {
              handleSubmitForm(e, action);
            }}>
            {(formik) => {
              const {
                errors,
                touched,
                setFieldValue,
                handleSubmit,
                handleChange,
                values,
              } = formik;
              return (
                <View style={Styles.formContainer}>
                  <Input
                    inputStyle={{color: 'white'}}
                    labelStyle={{color: 'white'}}
                    errorMessage={errors.title}
                    label="Title"
                    placeholder="Input Title"
                    onChangeText={handleChange('title')}
                    // onBlur={handleChange('title')}
                    value={values.title}
                  />
                  <Input
                    inputStyle={{color: 'white'}}
                    labelStyle={{color: 'white'}}
                    errorMessage={errors.publication}
                    label="Publication"
                    placeholder="Input Publication"
                    value={values.publication}
                    onChangeText={handleChange('publication')}
                    // onBlur={handleChange('publication')}
                  />
                  <Input
                    inputStyle={{color: 'white'}}
                    labelStyle={{color: 'white'}}
                    // onBlur={handleChange('pages')}
                    errorMessage={errors.pages}
                    label="Pages"
                    placeholder="Input Pages"
                    value={values.pages}
                    onChangeText={handleChange('pages')}
                  />
                  <Input
                    inputStyle={{color: 'white'}}
                    labelStyle={{color: 'white'}}
                    // onBlur={handleChange('ISBN')}
                    errorMessage={errors.ISBN}
                    label="ISBN"
                    placeholder="Input ISBN"
                    value={values.ISBN}
                    onChangeText={handleChange('ISBN')}
                  />
                  <Input
                    inputStyle={{color: 'white'}}
                    labelStyle={{color: 'white'}}
                    // onBlur={handleChange('author')}
                    errorMessage={errors.author}
                    label="Author"
                    placeholder="Input Author"
                    value={values.author}
                    onChangeText={handleChange('author')}
                  />
                  <Button
                    title={
                      file
                        ? file.name.substring(0, 10) + '.....'
                        : 'Select Image'
                    }
                    onPress={() => {
                      chooseFile();
                    }}
                    buttonStyle={{padding: 15, width: '40%', marginBottom: 30}}
                  />

                  <ButtonCustom loading={loading} onPress={handleSubmit} />
                </View>
              );
            }}
          </Formik>
        </View>
      </ScrollView>
    </View>
  );
}

const Styles = StyleSheet.create({
  container: {
    marginVertical: 20,
  },
  formContainer: {
    marginHorizontal: 10,
  },
});

export default AddScreen;
