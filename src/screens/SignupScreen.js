import {Formik} from 'formik';
import React, {useContext} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {Input} from 'react-native-elements';
import ButtonCustom from '../components/ButtonCustom';
import Logo from '../components/Logo';
import {LoginContext} from '../context/loginContext';
import * as Yup from 'yup';
import {API, headerConfig, setAuthToken} from '../config/api';
import {ScrollView} from 'react-native-gesture-handler';
import {Picker} from '@react-native-community/picker';
import {useState} from 'react';

function SignupScreen(props) {
  const [state, dispatch] = useContext(LoginContext);
  const [loading, setloading] = useState(false);
  const initialValues = {
    email: '',
    password: '',
    fullName: '',
    gender: '',
    phone: '',
    address: '',
  };

  const Schema = Yup.object().shape({
    email: Yup.string().required('Email Not Allow To be Empty').email(),
    password: Yup.string().required('Password Not Allow To be Empty'),
    fullName: Yup.string().required('Full Name Not Allow To be Empty'),
    gender: Yup.string().required('Gender Not Allow To be Empty'),
    phone: Yup.number().required('Phone Not Allow To be Empty'),
    address: Yup.string().required('Address Not Allow To be Empty'),
  });

  const handleSubmitForm = async (e, action) => {
    setloading(true);
    const body = JSON.stringify(e);
    console.log(body);
    try {
      const res = await API.post('/register', body, headerConfig);
      dispatch({
        type: 'LOGIN_SUCCESS',
        payload: res.data.data,
      });
      setAuthToken(res.data.data.token);
      // setemailUsed(false);
      setloading(false);
      try {
        const res = await API.get('/auth');
        dispatch({
          type: 'USER_LOADED',
          payload: res.data.data.user,
        });
        setloading(false);
      } catch (err) {
        dispatch({
          type: 'AUTH_ERROR',
        });
        setloading(false);
      }
    } catch (err) {
      dispatch({
        type: 'LOGIN_FAIL',
      });
      // setemailUsed(true);
      setloading(false);
    }
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <Formik
            initialValues={initialValues}
            validationSchema={Schema}
            onSubmit={(e, action) => {
              handleSubmitForm(e, action);
            }}>
            {(formik) => {
              const {
                errors,
                touched,
                setFieldValue,
                handleSubmit,
                handleChange,
                values,
              } = formik;
              return (
                <View>
                  <Text style={styles.textLogin}>SIGNUP</Text>
                  <Input
                    onBlur={handleChange('email')}
                    errorMessage={errors.email}
                    label="Email"
                    placeholder="Input Email"
                    onChangeText={handleChange('email')}
                    value={values.email}
                  />
                  <Input
                    onBlur={handleChange('password')}
                    errorMessage={errors.password}
                    label="Password"
                    placeholder="Input Password"
                    secureTextEntry={true}
                    value={values.password}
                    onChangeText={handleChange('password')}
                  />
                  <Input
                    onBlur={handleChange('fullName')}
                    errorMessage={errors.fullName}
                    label="Full Name"
                    placeholder="Input Name"
                    value={values.fullName}
                    onChangeText={handleChange('fullName')}
                  />
                  <Text
                    style={{
                      color: 'gray',
                      marginLeft: 10,
                      fontSize: 18,
                      fontWeight: 'bold',
                    }}>
                    Gender
                  </Text>
                  <Picker
                    selectedValue={values.gender}
                    onValueChange={(itemValue) =>
                      setFieldValue('gender', itemValue)
                    }
                    style={{color: 'gray', marginBottom: 0}}>
                    <Picker.Item label="Male" value="male" />
                    <Picker.Item label="Female" value="female" />
                  </Picker>
                  <Text
                    style={{
                      color: 'red',
                      marginTop: 0,
                      marginLeft: 14,
                      fontSize: 12,
                    }}>
                    {errors.gender}
                  </Text>
                  <Input
                    onBlur={handleChange('phone')}
                    errorMessage={errors.phone}
                    label="Phone"
                    placeholder="Input Phone"
                    value={values.phone}
                    onChangeText={handleChange('phone')}
                  />
                  <Input
                    onBlur={handleChange('address')}
                    errorMessage={errors.address}
                    label="Address"
                    placeholder="Input Address"
                    value={values.address}
                    onChangeText={handleChange('address')}
                  />
                  <ButtonCustom loading={loading} onPress={handleSubmit} />
                </View>
              );
            }}
          </Formik>
          <View style={{marginTop: 10}}>
            <Text style={{textAlign: 'center'}}>
              Already Have account ? Click{' '}
              <Text
                style={{color: 'red'}}
                onPress={() => props.navigation.navigate('Login')}>
                Here
              </Text>
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  textStyle: {
    color: 'white',
  },
  formContainer: {
    width: '95%',
    borderRadius: 4,
    backgroundColor: 'white',
    opacity: 0.9,
    paddingVertical: 30,
    paddingHorizontal: 15,
    marginTop: 20,
    marginBottom: 20,
  },
  logo: {
    marginBottom: 20,
  },
  textLogin: {
    fontSize: 30,
    textAlign: 'center',
    marginBottom: 25,
  },
});

export default SignupScreen;
