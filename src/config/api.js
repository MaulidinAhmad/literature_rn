import axios from 'axios';

export const publicUrl =
  'https://firebasestorage.googleapis.com/v0/b/literature-70cbb.appspot.com/o/';
export const imageUrl = 'https://res.cloudinary.com/dn6rtlztm/image/upload';
// export const baseBackendUrl = "http://localhost:5000/api/v1";
// export const publicUrl = "https://literature-api.herokuapp.com";

// Axios Default Url
export const baseBackendUrl = 'https://literature-api.herokuapp.com/api/v1';
// Axios Default Url
export const API = axios.create({
  baseURL: baseBackendUrl,
});

// Set Auth Token
export const setAuthToken = (token) => {
  if (token) {
    API.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  } else {
    delete API.defaults.headers.common['Authorization'];
  }
};

export const headerConfig = {
  headers: {
    'Content-Type': 'application/json',
  },
};
