import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import ProfileScreen from '../screens/ProfileScreen';
import HomeScreen from '../screens/HomeScreen';
import AddScreen from '../screens/AddScreen';
import Collections from '../screens/Collections/index';
import MyLiteratures from '../screens/MyLiteratures';

function MainBottomNavigators(props) {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        keyboardHidesTabBar: true,
        inactiveBackgroundColor: '#252525',
        activeBackgroundColor: '#252525',
        activeTintColor: '#AF2E1C',
        inactiveTintColor: 'white',
        style: {
          borderTopWidth: 1,
          borderTopColor: '#141414',
        },
      }}>
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({focused}) => (
            <Icon
              name="ios-person"
              color={focused ? '#AF2E1C' : 'white'}
              size={focused ? 28 : 20}
            />
          ),
        }}
      />
      <Tab.Screen
        name="My Literature"
        component={MyLiteratures}
        options={{
          tabBarLabel: 'My Literature',
          tabBarIcon: ({focused}) => (
            <Icon
              name="ios-list"
              color={focused ? '#AF2E1C' : 'white'}
              size={focused ? 28 : 20}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({focused}) => (
            <Icon
              name="ios-home"
              color={focused ? '#AF2E1C' : 'white'}
              size={focused ? 28 : 20}
            />
          ),
        }}
      />
      <Tab.Screen
        name="My Collection"
        component={Collections}
        options={{
          tabBarLabel: 'My Collection',
          tabBarIcon: ({focused}) => (
            <Icon
              name="ios-list"
              color={focused ? '#AF2E1C' : 'white'}
              size={focused ? 28 : 20}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Add"
        component={AddScreen}
        options={{
          tabBarLabel: 'Add',
          tabBarIcon: ({focused}) => (
            <Icon
              name="ios-create"
              color={focused ? '#AF2E1C' : 'white'}
              size={focused ? 28 : 20}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default MainBottomNavigators;
