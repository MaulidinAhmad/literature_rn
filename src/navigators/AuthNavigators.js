import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from '../screens/LoginScreen';
import SignupScreen from '../screens/SignupScreen';
import SplashScreen from '../screens/SplashScreen';

const AuthStack = createStackNavigator();
const LoginStack = createStackNavigator();

function AuthNavigators(props) {
  return (
    <AuthStack.Navigator
      mode={'modal'}
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="LoginStack">
      <AuthStack.Screen
        name={'Login'}
        component={LoginScreen}></AuthStack.Screen>
      <AuthStack.Screen name={'Signup'} component={SignupScreen} />
    </AuthStack.Navigator>
  );
}

export default AuthNavigators;
